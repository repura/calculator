﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string Input_str = "";//入力された数字
        double Result = 0;//計算結果
        string Operator = null;//押された演算子

        private void ButtonClicked(object sender, EventArgs e)
        {
            //senderの詳しい情報を取り扱えるようにする
            Button button = (Button)sender;

            //押されたボタンの数字
            string text = button.Text;

            //入力された数字に連結する
            Input_str += text;

            //画面に出す
            textBox1.Text = Input_str;
           
        }

       

        private void ProcessingButton(object sender, EventArgs e)
        {
            double num1 = Result;//現在の結果
            double num2;        //入力された文字

            //入力された文字が空欄なら、計算をスキップ
            if (Input_str != "")
            {
                num2 = double.Parse(Input_str);


                //四則演算
                if (Operator == "+")
                    Result = num1 + num2;

                if (Operator == "-")
                    Result = num1 - num2;

                if (Operator == "*")
                    Result = num1 * num2;

                if (Operator == "/")
                    Result = num1 / num2;

                //演算子を押されていなかった場合
                if (Operator == null)
                    Result = num2;
            }
            //画面に計算結果を表示
                textBox1.Text = Result.ToString();

            //入力されてる数字をリセット
                Input_str = "";

            //演算子をoperator変数に入れる
                Button button = (Button)sender;
                Operator = button.Text;

                if (Operator == "=")
                    Operator = "";
            
        }

        
            


        }
    }

